package demo.com.progressiveimageview;

import android.animation.ValueAnimator;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

public class DataUsageAdapters extends RecyclerView.Adapter<DataViewHolder> {
    List<Integer> mPercentageValues = new ArrayList<>();

    public DataUsageAdapters(List<Integer> mPercentageValues) {
        this.mPercentageValues = mPercentageValues;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new DataViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_progressive_image, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder dataViewHolder, int i) {
        animateView(dataViewHolder.mBackSupport, mPercentageValues.get(i));
        dataViewHolder.txtPercentage.setText(mPercentageValues.get(i) + "%");
    }

    private void animateView(final ImageView imageView, int percentageValue) {
        int maxValue = getMaxValue();
        int maxHeight = (int) imageView.getContext().getResources().getDimension(R.dimen.data_usage_view_height);
        int minHeight = (int) imageView.getContext().getResources().getDimension(R.dimen.data_usage_view_start_height);
        int targetHeight = (percentageValue * maxHeight) / maxValue;

        final ValueAnimator anim = ValueAnimator.ofInt(minHeight, targetHeight + minHeight);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                final int val = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = imageView.getLayoutParams();
                layoutParams.height = val;
                imageView.setLayoutParams(layoutParams);
            }
        });
        anim.setDuration(1500);
        anim.start();
    }

    private int getMaxValue() {
        int maxValue = 0;
        for (Integer percentageValue : mPercentageValues) {
            if (percentageValue > maxValue) {
                maxValue = percentageValue;
            }
        }
        return maxValue;
    }

    @Override
    public int getItemCount() {
        return mPercentageValues.size();
    }
}
