package demo.com.progressiveimageview;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

class DataViewHolder extends RecyclerView.ViewHolder {
    public ImageView mBackSupport;
    public TextView txtPercentage, txtName;

    public DataViewHolder(@NonNull View itemView) {
        super(itemView);
        mBackSupport = itemView.findViewById(R.id.back_support);
        txtName = itemView.findViewById(R.id.txtName);
        txtPercentage = itemView.findViewById(R.id.txtPercentage);
    }
}
