package demo.com.progressiveimageview;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.dataUsageList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        List<Integer> integerList = new ArrayList<>();
        integerList.add(78);
        integerList.add(74);
        integerList.add(70);
        integerList.add(60);
        integerList.add(52);
        integerList.add(42);
        integerList.add(37);
        integerList.add(21);
        integerList.add(10);
        integerList.add(5);
        recyclerView.setAdapter(new DataUsageAdapters(integerList));
    }


    @Override
    protected void onResume() {
        super.onResume();
    }
}
